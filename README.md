# Evaluation of performances of netkit emulator #

misurare occupazione RAM e CPU per diversi scenari (lab netkit emulati)

esempio: una catena lineare di N router con un nodo client e un server ai bordi della catena
N=2 .. 8 ? (o quanti ne riuscite a mettere)

o vi preparate a mano 3 / 4 lab ad N crescente oppure preparate un "generatore di lab" che prende in input N e sceglie l'indirizzamento in modo automatico...

Ambiente VirtualBox

strumenti per prendere misure di RAM e CPU es. comando TOP linux... verificare se l'informazione di quanto consuma netkit e' concentrata in un singolo processo

Comando TOP: http://linux.about.com/od/commands/l/blcmdl1_top.htm
             http://www.ubuntu-linux.it/comando-top-significato-dei-valori/

strumenti per generare traffico tra client e server : iperf in UDP mode

http://www.slashroot.in/iperf-how-test-network-speedperformancebandwidth


estrarre l'informazione che varia nel tempo da top e metterla in dei file da post processare


ISTRUZIONI IPERF


http://ftp.debian.org/debian/pool/main/i/iperf/iperf_2.0.4-5_i386.deb
[ versione di iperf: iperf version 2.0.4 (7 Apr 2008) pthreads ]

Una volta scaricato per installarlo in una o piu macchine netkit che fanno parte del vostro progetto, è sufficiente inserirlo nella root del progetto stesso (dove c'è il file lab.conf).
In questo modo da ogni macchina netkit che viene lanciata è possibile accedervi tramite /hostlab.

Per installarlo basta digitare (all'interno della macchina netkit):

cd /hostlab
dpkg -i iperf_2.0.4-5_i386.deb

USARE IPERF http://www.ce.unipr.it/~bompani/iperf.html



## Instructions ##
The repository is organized as follows:

* documents -> all project documentation
* sources -> source code (please create subfolders as needed)
* other -> every other thing

The documents folder is organized in subfolders

* report -> main report document
* presentation -> presentation
* notes -> please add any notes, intermediate results
* related -> you can keep here external documents